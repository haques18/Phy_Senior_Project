import numpy as np
import math
import matplotlib.pyplot as plt
from matplotlib.patches import Circle

k = 9E9
q = np.array([])
qx = np.array([])
qy = np.array([])

def onAddButtonPress(newq, newx, newy):
	global q, qx, qy
	q = np.append(q, newq)
	qx = np.append(qx, newx)
	qy = np.append(qy, newy)

def figureSetup():
	fig = plt.figure()
	ax = fig.add_subplot(111)
	charge_colors = {True: '#aa0000', False: '#0000aa'}
	for i in range(len(q)):
		pos = tuple((qx[i], qy[i]))
		ax.add_artist(Circle(pos, 0.05, color = charge_colors[q[i]>0]))
	cid = fig.canvas.mpl_connect('button_press_event', onMouseClick)
	return ax

def axisSetup():
	cmin = np.min(qx) if np.min(qx) < np.min(qy) else np.min(qy)
	cmax = np.max(qx) if np.max(qx) > np.max(qy) else np.max(qy)
	xs = np.arange(cmin - 1, cmax + 1, .05)
	ys = np.arange(cmin - 1, cmax + 1, .05)
	x, y = np.meshgrid(xs, ys)
	return x, y

def eMeshgrid(x, y):
	ex = np.linspace(0, 0, len(x))
	ey = np.linspace(0, 0, len(y))
	for i in range(len(q)):
		r = np.hypot(x - qx[i], y - qy[i])
		minIndex = np.argmin(r)
		minx = minIndex // len(r)
		miny = minIndex % len(r)
		r[minx][miny] = np.hypot(.05, .05)
		ex = ex + (k * q[i] * (x-qx[i]) /(r**1.5))
		ey = ey + (k * q[i] * (y-qy[i]) /(r**1.5))
	return ex, ey

def vMeshgrid(x, y):
	v = np.linspace(0, 0, len(x))
	for i in range(len(q)):
		r = np.hypot(x - qx[i], y - qy[i])
		minIndex = np.argmin(r)
		minx = minIndex // len(r)
		miny = minIndex % len(r)
		r[minx][miny] = np.hypot(.05, .05)
		v = v + ((k * q[i])/ r)
	return v

def EFieldPlotter(x, y, ex, ey, plot):
	color = 2 * np.log(np.hypot(ex, ey))
	plot.streamplot(x, y, ex, ey, color = 'green', linewidth = 1, cmap = plt.cm.inferno,
              density = 2, arrowstyle = '->', arrowsize = 1.5)

def VFieldPlotter(x, y, v, plot):
	c = plot.contour(x,y,v, len(v))
	plt.colorbar(c)

def onEFieldButtonPress():
	plot = figureSetup()
	x, y = axisSetup()
	ex, ey = eMeshgrid(x, y)
	EFieldPlotter(x, y, ex, ey, plot)
	plt.show()

def onVGradButtonPress():
	plot = figureSetup()
	x, y = axisSetup()
	v = vMeshgrid(x, y)
	VFieldPlotter(x, y, v, plot)
	plt.show()

def onEFandVPlotButtonPress():
	plot = figureSetup()
	x, y = axisSetup()
	ex, ey = eMeshgrid(x, y)
	v = vMeshgrid(x, y)
	EFieldPlotter(x, y, ex, ey, plot)
	VFieldPlotter(x, y, v, plot)
	plt.show()

def VandECalc(x, y):
	v = 0
	ex = 0
	ey = 0
	for i in range(len(q)):
		r = np.hypot(x - qx[i], y - qy[i])
		v = v + ((k * q[i]) / r)
		ex = ex + (k * q[i] * (x - qx[i]) /(r**1.5))
		ey = ey + (k * q[i] * (y - qy[i]) /(r**1.5))
	ang = np.degrees(np.arctan(np.absolute(ey)/np.absolute(ex)))
	e = np.hypot(ex, ey)
	return v, ex, ey, ang, e

def onCalcButtonPress(xc, yc):
	return VandECalc(xc, yc)



def onMouseClick(event):
	v, ex, ey, ang, e = VandECalc(event.xdata, event.ydata)
	print(v)
	print(ex)
	print(ey)
	print(ang)
	print(e)