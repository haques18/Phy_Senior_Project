import kivy
kivy.require('1.0.6')

import potential as Est

from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.togglebutton import ToggleButton
# from kivy.garden.graph import Graph


class initSetup(FloatLayout):

    def __init__(self, **kwargs):
        super(initSetup, self).__init__(**kwargs)
        self.add_widget(Label(text = 'Charge: ', size_hint = (.15, .05), pos_hint = {'x': 0.05, 'y': .90}))
        self.charge = TextInput(multiline = False, size_hint = (.15, .05), pos_hint = {'x': 0.22, 'y': .90})
        self.add_widget(self.charge)
        self.add_widget(Label(text = 'X - coordinate: ', size_hint = (.15, .05), pos_hint = {'x': 0.05, 'y': .80}))
        self.xcc = TextInput(multiline = False, size_hint = (.15, .05), pos_hint = {'x': 0.22, 'y': .80})
        self.add_widget(self.xcc)
        self.add_widget(Label(text = 'Y - coordinate: ', size_hint = (.15, .05), pos_hint = {'x': 0.05, 'y': .70}))
        self.ycc = TextInput(multiline = False, size_hint = (.15, .05), pos_hint = {'x': 0.22, 'y': .70})
        self.add_widget(self.ycc)

        self.addBtn = Button(text = "Add", size_hint = (.15, .05), pos_hint = {'x': 0.22, 'y': .60})
        self.add_widget(self.addBtn)
        self.addBtn.bind(on_press = self.addBtnCallback)


        self.EFVecTog = Button(text = 'Plot the Electric Field Vector',
            size_hint = (.35, .05), pos_hint = {'x': .55, 'y': .9})
        self.add_widget(self.EFVecTog)
        self.VGradTog = Button(text = 'Plot the Electric Potential Gradient',
            size_hint = (.35, .05), pos_hint = {'x': .55, 'y': .8})
        self.add_widget(self.VGradTog)
        # self.ECalcTog = Button(text = 'Calculate the Electric Energy',
        #     size_hint = (.35, .05), pos_hint = {'x': .55, 'y': .5})
        # self.add_widget(self.ECalcTog)
        # self.FCalcTog = Button(text = 'Calculate the Electric Force',
        #     size_hint = (.35, .05), pos_hint = {'x': .55, 'y': .4})
        # self.add_widget(self.FCalcTog)
        self.EFandVPlotTog = Button(text = 'Plot the Electric Field \n and Potential Gradient',
            size_hint = (.35, .10), pos_hint = {'x': .55, 'y': .65})
        self.add_widget(self.EFandVPlotTog)
        
        self.EFVecTog.bind(on_press = self.EFVecTogCallback)
        self.VGradTog.bind(on_press = self.VGradTogCallback)
        # self.ECalcTog.bind(on_press = self.ECalcTogCallback)
        # self.FCalcTog.bind(on_press = self.FCalcTogCallback)
        self.EFandVPlotTog.bind(on_press = self.EFandVPlotTogCallback)

        self.add_widget(Label(text = 'Calculate the Electric potential and Field strength at position: ',
            size_hint = (.45, .05), pos_hint = {'x': 0.10, 'y': .10}))
        self.add_widget(Label(text = 'X: ', size_hint = (.05, .05), pos_hint = {'x': 0.10, 'y': .05}))
        self.xcInput = TextInput(multiline = False, size_hint = (.1, .05), pos_hint = {'x': 0.15, 'y': .05})
        self.add_widget(self.xcInput)
        self.add_widget(Label(text = 'Y: ', size_hint = (.05, .05), pos_hint = {'x': 0.26, 'y': .05}))
        self.ycInput = TextInput(multiline = False, size_hint = (.1, .05), pos_hint = {'x': 0.31, 'y': .05})
        self.add_widget(self.ycInput)
        self.calcBtn = Button(text = "Calculate", size_hint = (.15, .05), pos_hint = {'x': 0.45, 'y': .05})
        self.add_widget(self.calcBtn)
        self.calcBtn.bind(on_press = self.calcBtnCallback)

        self.potLabel = Label(text = '',
            size_hint = (1, None), pos_hint = {'x': 0.05, 'y': .35}, halign = "left")
        self.add_widget(self.potLabel)
        self.eVecLabel = Label(text = '', size_hint = (1, None), 
            pos_hint = {'x': 0.05, 'y': .30}, halign = 'left')
        self.add_widget(self.eVecLabel)
        self.eAngleLabel = Label(text = '', 
            size_hint = (1, None), pos_hint = {'x': 0.05, 'y': .25}, halign = 'left') 
        self.add_widget(self.eAngleLabel)
        self.eStrengthLabel = Label(text = '', size_hint = (1, None), 
            pos_hint = {'x': 0.05, 'y': .20}, halign = 'left')
        self.add_widget(self.eStrengthLabel)


    def addBtnCallback(self, value):
        q = float(self.charge.text)
        x = float(self.xcc.text)
        y = float(self.ycc.text)
    	Est.onAddButtonPress(q, x, y)
        self.charge.text = ""
        self.xcc.text = ""
        self.ycc.text = ""


    def EFVecTogCallback(self, value):
        Est.onEFieldButtonPress()


    def VGradTogCallback(self, value):
        Est.onVGradButtonPress()

    # def ECalcTogCallback(self, value):
    #     return

    # def FCalcTogCallback(self, value):
    #     return

    def EFandVPlotTogCallback(self, value):
        Est.onEFandVPlotButtonPress()

    def calcBtnCallback(self, value):
        x = float(self.xcInput.text)
        y = float(self.ycInput.text)
        v, ex, ey, ang, e = Est.onCalcButtonPress(x, y)
        self.potLabel.text = 'Potential: ' + str(v)
        self.eVecLabel.text = 'Electric Field Vector: ' + "<" + str(ex) + ", " + str(ey) + ">"
        self.eAngleLabel.text = 'Electric Field angle, counter clockwise from x-axis: ' + str(ang)
        self.eStrengthLabel.text = 'Electric Field Strength: ' + str(e)

        self.xcInput.text = ""
        self.ycInput.text = ""


class electrostaticsGuiApp(App):

    def build(self):
        return initSetup()


if __name__ == '__main__':
    electrostaticsGuiApp().run()