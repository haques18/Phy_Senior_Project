import kivy
kivy.require('1.0.6')

import potential as Est

from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.togglebutton import ToggleButton

from math import sin
from kivy.garden.graph import Graph, MeshLinePlot


class initSetup(FloatLayout):

    def __init__(self, **kwargs):
        super(initSetup, self).__init__(**kwargs)
        self.add_widget(Label(text = 'Add Charge: ', size_hint = (.1, .05), pos_hint = {'x': 0.01, 'y': .15}))
        self.charge = TextInput(multiline = False, size_hint = (.1, .05), pos_hint = {'x': 0.12, 'y': .15})
        self.add_widget(self.charge)
        self.add_widget(Label(text = 'X - coordinate: ', size_hint = (.1, .05), pos_hint = {'x': 0.25, 'y': .15}))
        self.xcc = TextInput(multiline = False, size_hint = (.1, .05), pos_hint = {'x': 0.36, 'y': .15})
        self.add_widget(self.xcc)
        self.add_widget(Label(text = 'Y - coordinate: ', size_hint = (.1, .05), pos_hint = {'x': 0.49, 'y': .15}))
        self.ycc = TextInput(multiline = False, size_hint = (.1, .05), pos_hint = {'x': 0.6, 'y': .15})
        self.add_widget(self.ycc)

        self.goBtn = Button(text = "Plot", size_hint = (.1, .05), pos_hint = {'x': 0.73, 'y': .15})
        self.add_widget(self.goBtn)
        self.goBtn.bind(on_press = self.goBtnCallback)

        self.EFCalcTog = ToggleButton(text = 'Calculate the Electric Field', group = 'main',
            size_hint = (.2, .05), pos_hint = {'x': 0.775, 'y': .9})
        self.add_widget(self.EFCalcTog)
        self.EFVecTog = ToggleButton(text = 'Plot the Electric Field Vector', group = 'main',
            size_hint = (.2, .05), pos_hint = {'x': 0.775, 'y': .8})
        self.add_widget(self.EFVecTog)
        self.VCalcTog = ToggleButton(text = 'Calculate the Electric Potential', group = 'main',
            size_hint = (.2, .05), pos_hint = {'x': 0.775, 'y': .7})
        self.add_widget(self.VCalcTog)
        self.VGradTog = ToggleButton(text = 'Plot the Electric Potential Gradient', group = 'main',
            size_hint = (.2, .05), pos_hint = {'x': 0.775, 'y': .6})
        self.add_widget(self.VGradTog)
        self.ECalcTog = ToggleButton(text = 'Calculate the Electric Energy', group = 'main',
            size_hint = (.2, .05), pos_hint = {'x': 0.775, 'y': .5})
        self.add_widget(self.ECalcTog)
        self.FCalcTog = ToggleButton(text = 'Calculate the Electric Force', group = 'main',
            size_hint = (.2, .05), pos_hint = {'x': 0.775, 'y': .4})
        self.add_widget(self.FCalcTog)
        
        self.EFCalcTog.bind(on_press = self.EFCalcTogCallback)
        self.EFVecTog.bind(on_press = self.EFVecTogCallback)
        self.VCalcTog.bind(on_press = self.VCalcTogCallback)
        self.VGradTog.bind(on_press = self.VGradTogCallback)
        self.ECalcTog.bind(on_press = self.ECalcTogCallback)
        self.FCalcTog.bind(on_press = self.FCalcTogCallback)

        self.graph = Graph(xlabel='X', ylabel='Y', x_ticks_minor=5,
            x_ticks_major=25, y_ticks_major=1,
            y_grid_label=True, x_grid_label=True, padding=5,
            x_grid=True, y_grid=True, xmin=-0, xmax=100, ymin=-1, ymax=1,
            size_hint = (.75, .75), pos_hint = {'x': 0, 'y': .25})
        self.add_widget(self.graph)


    def goBtnCallback(self, value):
    	return self.add_widget(Label(text = "ButtonPressed"))

    def EFCalcTogCallback(self, value):
        return

    def EFVecTogCallback(self, value):
        return

    def VCalcTogCallback(self, value):
        return

    def VGradTogCallback(self, value):
        return

    def ECalcTogCallback(self, value):
        return

    def FCalcTogCallback(self, value):
        return


class electrostaticsGuiApp(App):

    def build(self):
        return initSetup()


if __name__ == '__main__':
    electrostaticsGuiApp().run()